use std::env;

fn main() {
  println!("👋 hello world 🌍");
  println!("Args:");
  for argument in env::args().skip(1) {
    println!("{}", argument);
  }
}
