# WASI & Rust

```bash
rustup target add wasm32-wasi # try to add it to the container
cargo new --lib hello
cd hello
cargo build --target wasm32-wasi

# see: target/wasm32-wasi/debug/add.wasm
wasmedge --reactor target/wasm32-wasi/debug/add.wasm add 2 2
wasmtime target/wasm32-wasi/debug/add.wasm --invoke add 1 2
```
